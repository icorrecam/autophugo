# References

i18n

- [I18n Tutorial: How to Go Multilingual with Hugo](https://phrase.com/blog/posts/i18n-tutorial-how-to-go-multilingual-with-hugo/)
- …

Microdata:

- [ImageObject Schema Generator and Guide](https://schemantra.com/schema_list/ImageObject)
- [Schema Generator](https://schemagen.scas.io/)
- …
