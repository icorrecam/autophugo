---
{{- $imgglob := printf "*%s" (path.Join .File.Dir "*") -}}
{{- $resources := where (resources.Match $imgglob) "ResourceType" "image" }}
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date | time.Format ":date_full" }}
weight: 0
# authors: []
# albumdate: {{ .Date | time.Format ":date_full" }}
albumthumb: "{{ cond (gt (len $resources) 0) (index $resources 0) "" }}"
draft: false
## Optional additional meta info for resources list
#  alt: Image alternative and screen-reader text
#  phototitle: A title for the photo
#  description: A sub-title or description for the photo
resources:
{{ range $elem_index, $elem_val := $resources -}}
- src: "{{ . }}"
  draft: false
  weight: {{ add (mul $elem_index 10) 10 }}
  phototitle:  #
    «SUSTITUIR» Título de la imagen
  description:  #
    «SUSTITUIR» Descripción de la imagen.
    Puede ocupar varias líneas, si se quiere.
    Esta sería una tercera línea…
  alt:  #
    «SUSTITUIR» Descripción alternativa de la imagen.
    Se utiliza para complementar la información visual.
    Esta sería una tercera línea…
  categories:
    - Categoría1
    - Categoría2
    - Categoría3
  tags:
    - Etiqueta1
    - Etiqueta2
    - Etiqueta3

{{ end -}}
---
