EXAMPLE_SITE := exampleSite
CONFIG_GH_PAGES := config_for_github_pages.toml
CONFIG := config.toml
THEMES := ../../

.PHONY: public
public:
	hugo -s ${EXAMPLE_SITE} --config ${CONFIG_GH_PAGES} --themesDir ${THEMES}

.PHONY: server
server:
	# hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} --debug -D server
	clear && ./hugo -s ${EXAMPLE_SITE} --config hugo.toml --themesDir ${THEMES} --logLevel debug server  --disableFastRender --gc --printI18nWarnings --printPathWarnings --printUnusedTemplates

.PHONY: testAlbum
testAlbum:
	hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} new test/_index.md

.PHONY: testAlbum2
testAlbum2:
	hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} new test/test/_index.md

.PHONY: testAlbum3
testAlbum3:
	hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} new test/test/test/_index.md

.PHONY: testAlbum4
testAlbum4:
	hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} new test/test/test/test/_index.md

.PHONY: testDelete
testDelete:
	# rm -rf ${EXAMPLE_SITE}/assets/test
	rm -rf ${EXAMPLE_SITE}/content/test
	rm -rf ${EXAMPLE_SITE}/resources/_gen

.PHONY: serverNoAlbum
serverNoAlbum:
	hugo -s exampleSiteNoAlbum --themesDir ${THEMES} server

.PHONY: no_canonify_server_long_url_test
no_canonify_server_long_url_test:
	hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} server -b http://localhost:1313/themes/autophugo/

.PHONY: canonify_server_long_url_test
canonify_server_long_url_test:
	export HUGO_CANONIFYURLS=true && hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} server -b http://localhost:1313/themes/autophugo/

.PHONY: no_canonify_server_short_url_test
no_canonify_server_short_url_test:
	hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} server -b http://localhost:1313/

.PHONY: canonify_server_short_url_test
canonify_server_short_url_test:
	export HUGO_CANONIFYURLS=true && hugo -s ${EXAMPLE_SITE} --themesDir ${THEMES} server -b http://localhost:1313/


.PHONY: all
